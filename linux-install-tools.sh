##### REPOSITORIES ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### 

sudo add-apt-repository -y ppa:git-core/ppa
sudo add-apt-repository -y ppa:webupd8team/java
#sudo apt-add-repository -y ppa:cordova-ubuntu/ppa
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
sudo sh -c 'echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'

##### UPDATE REPOSITORIES ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### 
sudo apt-get update
sudo apt-get upgrade
#sudo apt-get dist-upgrade

##### INSTALL ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####

# Basic linux build tools
sudo apt-get install -y build-essential make
sudo apt-get install -y python-software-properties
sudo apt-get install -y ubuntu-restricted-extras

# Others
sudo apt-get install -y debconf-utils
sudo apt-get install -y python-software-properties
sudo apt-get install -y ntp ntfs-config
sudo apt-get install -y gimp gimp-plugin-registry
sudo apt-get install -y htop
sudo apt-get install -y curl
sudo apt-get install -y hwinfo
sudo apt-get install -y meld
sudo apt-get install -y vim
sudo apt-get install -y whois
sudo apt-get install -y openssh-server openssh-client
sudo apt-get install -y network-manager-vpnc network-manager-vpnc-gnome
sudo apt-get install -y openvpn

# Git
sudo apt-get install -y git git-gui
git config --global user.name "ifnazar" 
git config --global user.email "ifnazar@gmail.com" 
git config --global push.default simple
#git config branch.autosetuprebase always # Force all new branches to automatically use rebase


# Java
echo "oracle-java8-installer shared/accepted-oracle-license-v1-1 select true" | sudo debconf-set-selections
sudo apt-get install -y oracle-java7-installer
sudo apt-get install -y oracle-java8-installer
sudo update-java-alternatives -s java-8-oracle

# NPM modules
sudo apt-get install -y npm
sudo npm install -g bower grunt-cli

# nvm, node and modules
sudo npm install -g n
sudo n 6.9.1 
sudo n 8.2.1 
sudo n 8.8.1

## http://nodegh.io/
sudo npm install gh -g

## gulp
sudo npm install -g yo gulp

## scss
sudo apt install ruby -y
sudo apt install ruby-sass -y

## ngrok
sudo npm install ngrok -g

# Cordova
#sudo apt-get install -y cordova-cli
#sudo npm install -g cordova
#sudo apt-get install -y cmake debhelper libx11-dev libicu-dev pkg-config qtbase5-dev qtchooser qtdeclarative5-dev qtfeedback5-dev qtlocation5-dev qtmultimedia5-dev qtpim5-dev qtsensors5-dev qtsystems5-dev

## dbeaver
cd /tmp
wget http://dbeaver.jkiss.org/files/dbeaver-ce_latest_amd64.deb
sudo dpkg -i dbeaver-ce_latest_amd64.deb

# Chrome
sudo apt-get install -y google-chrome-stable

# Maven
sudo apt-get install -y maven

## Gradle
sudo apt-get install -y gradle

## Virtualbox
sudo apt-get install -y virtualbox

# DOCKER
sudo wget -qO- https://get.docker.com/ | sh
sudo usermod -aG docker $USER
DOCKER_FILE="docker-compose-`uname -s`-`uname -m`"
echo $DOCKER_FILE
wget https://github.com/docker/compose/releases/download/1.21.2/"$DOCKER_FILE"
sudo mv "$DOCKER_FILE" /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
sudo usermod -aG docker $USER
sudo gpasswd -a ${USER} docker
sudo service docker restart

##### CUSTOM INSTALL ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####

mkdir ~/desenvolvimento
mkdir ~/programas
cd ~/programas

# WEBSTORM
#wget https://download.jetbrains.com/webstorm/WebStorm-11.0.3.tar.gz
#tar xvzf WebStorm-11.0.3.tar.gz
#mv WebStorm-143.1559.5 webStorm
#rm -rf *.gz

# ECLIPSE JEE
#wget http://eclipse.c3sl.ufpr.br/technology/epp/downloads/release/mars/2/eclipse-java-mars-2-linux-gtk-x86_64.tar.gz
#tar xvzf eclipse-java-mars-2-linux-gtk-x86_64.tar.gz
#mv eclipse eclipse-mars
#rm -rf *.gz

# ECLIPSE JEE
#wget http://eclipse.c3sl.ufpr.br/technology/epp/downloads/release/mars/2/eclipse-jee-mars-2-linux-gtk-x86_64.tar.gz
#tar xvzf eclipse-jee-mars-2-linux-gtk-x86_64.tar.gz
#mv eclipse eclipse-jee-mars
#rm -rf *.gz

# ANDROID SDK
#wget http://dl.google.com/android/adt/22.6.2/adt-bundle-linux-x86_64-20140321.zip 
#unzip adt-bundle-linux-x86_64-20140321.zip
#mv adt-bundle-linux-x86_64-20140321/sdk android_sdk
#rm -rf adt-bundle-linux-x86_64-20140321*

# sudo add-apt-repository ppa:paolorotolo/android-studio
# sudo apt-get update
# sudo apt-get install android-studio


##### FIX ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####
# Config SSH
mkdir ~/.ssh
chmod 700 ~/.ssh
#ssh-keygen -t rsa

# Disable middle mouse button click paste
echo pointer = 1 25 3 4 5 6 7 8 9  >>  ~/.Xmodmap

#FIX ECLIPSE GTK - UBUNTU 16
echo export SWT_GTK3=0 >> ~/.profile
