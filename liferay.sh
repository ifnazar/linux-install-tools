#################################### Liferay
# ECLIPSE JEE
rm -rf  ~/programas/eclipse-liferay
cd /tmp
wget https://ufpr.dl.sourceforge.net/project/lportal/Liferay%20IDE/3.1.2%20GA3/liferay-ide-eclipse-linux-x64-3.1.2-ga3-201709011126.tar.gz
tar xvzf liferay-ide-eclipse*.tar.gz
mv eclipse  ~/programas/eclipse-liferay

## Blade
# curl https://raw.githubusercontent.com/liferay/liferay-blade-cli/master/installers/global | sudo sh
curl https://raw.githubusercontent.com/liferay/liferay-blade-cli/master/cli/installers/global | sudo sh

## Generator Liferay Theme
sudo npm install -g generator-liferay-theme

## jpm4j
cd /tmp
curl -sL https://github.com/jpm4j/jpm4j.installers/raw/master/dist/biz.aQute.jpm.run.jar >jpm4j.jar
java -jar jpm4j.jar -u init
sudo ln -s $HOME/jpm/bin/jpm /usr/bin/jpm


# LOMBOK
#wget https://projectlombok.org/downloads/lombok.jar
#java -jar lombok.jar install ${HOME}/programas/eclipse-liferay
#rm -rf lombok.jar

####################################